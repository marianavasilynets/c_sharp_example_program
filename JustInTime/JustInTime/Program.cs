﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JustInTime
{
    class Program
    {
        static void Main(string[] args)
        {
            AnaliticModel model = new AnaliticModel(1, 2, 3.5, 4.5, 1, 0);
            Console.Write("Середнє значення часу логiстичного циклу: ");
            Console.WriteLine(model.AverageTime());
            Console.WriteLine();
            Console.Write("Середнє квадратичне вiдхилення: ");
            Console.WriteLine(model.StandartDevitation);          
            Console.Write("Час виконання циклу: ");
            Console.WriteLine("14 днiв");
            Console.Write("Iмовiрнiсть виконання: ");
            Console.WriteLine(model.Probability(14));
            Console.Write("Iмовiрнiсть виконання на iнтервалi [11;17]: ");
            double a = model.ProbabilityInterSpace(17, 11);
            Console.WriteLine(a);
            Console.ReadKey();
        }
    }
}
