﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JustInTime
{
    class AnaliticModel
    {
        public double transference;
        public double processing;
        public double completing;
        public double carriage;
        public double delivery;
        public double expect;
        public double[,] korr;

        //Організація доступу до полів

        public double this[int i]
        {
            get
            {
                switch(i)
                {
                    case 0: return transference;
                        break;
                    case 1: return processing;
                        break;
                    case 2: return completing;
                        break;
                    case 3: return carriage;
                        break;
                    case 4: return delivery;
                        break;
                }
                return this[i];
            }
        }

        public AnaliticModel()
        {
            transference = processing = completing = carriage = delivery = expect = 0;
        }

        public AnaliticModel(double transference, double processing, double completing, double carriage, double delivery, double expect)
        {
            this.transference = transference;
            this.processing = processing;
            this.completing = completing;
            this.carriage = carriage;
            this.delivery = delivery;
            this.expect = expect;
            try
            {
                double [,]Korr=new double[5,5];
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        if (i == j) Korr[i, j] = 1;
                        else if (i > j) Korr[i, j] = 0;
                        else
                        {
                            Console.Write("korr[{0},{1}]=", i+1, j+1);
                            Korr[i, j] = double.Parse(Console.ReadLine());
                        }
                        if (Korr[i, j] > 1 || Korr[i, j] < -1) throw new Exception();
                    }
                }
                this.korr = Korr;
            }

            catch(NullReferenceException)
            {

            }

            catch (Exception)
            {
                Console.WriteLine("Коефіцієнт повинен бути в межах [-1;1]");
            }

            
        }

        public AnaliticModel(AnaliticModel model)
        {
            this.transference = model.transference;
            this.processing = model.processing;
            this.completing = model.completing;
            this.carriage = model.carriage;
            this.delivery = model.delivery;
            this.expect = model.expect;
            this.korr = model.korr;
        }

        //Середнє значення часу логістичного циклу
        public double AverageTime()
        {
            return transference + processing + completing + carriage + delivery;
        }

        //Середнє арифметичне вибірки
        public double averageSample
        {
            get
            {
                double averageSample = 0;
                for (int i = 0; i < 5; i++)
                {
                    averageSample += this[i];
                }
                return averageSample / 5;
            }
        }

        //Отримання квадратичного відхилення операції
        public double getDeviation(int index)
        {      
            return this[index] - this.averageSample;
        }

        public void setDevitation(int index, double[] value)
        {
            double dev;
            switch(index)
            {
                case 0: dev = value[0];
                    break;
                case 1: dev = value[1];
                    break;
                case 2: dev = value[2];
                    break;
                case 3: dev = value[3];
                    break;
                case 4: dev = value[4];
                    break;
            }
        }

        //Задання кореляційної матриці

        //Середнє квадратичне відхилення
        public double StandartDevitation
        {
            get
            {
                    double dev = 0;
                    for (int i = 0; i < 5; i++)
                    {
                        dev += Math.Pow(this.getDeviation(i), 2);
                    }
                    double[,] CorrelationMatrix = this.korr;
                    double korr = 0;
                    for (int i = 0; i < 5; i++)
                    {
                        for (int j = 0; j < 5; j++)
                        {
                            if (i <= j)
                                korr += CorrelationMatrix[i, j] * this.getDeviation(i) * this.getDeviation(j);
                        }

                    }
                    korr *= 2;
                    return Math.Round(Math.Sqrt(dev + korr), 2);
                }
            }

        public double NormalDistribution()
        {
            double func = this.exp((this.averageSample - expect) / StandartDevitation);
            return SimpsonRule(Double.NegativeInfinity, this.averageSample, 20) / (this.StandartDevitation * Math.Sqrt(2 * Math.PI));
        }

        //Верхня довірча межа часу
        public double ConfidentialUpperLimit()
        {
            return this.AverageTime() + this.NormalDistribution()*this.StandartDevitation;
        }

        //Час виконання циклу
        public double TimeChain(double StartTime)
        {
            return StartTime + this.ConfidentialUpperLimit();
        }

        //Нижня довірча межа
        public double TimeChainLowerLim(double StartTime)
        {
            return StartTime + this.AverageTime() - this.NormalDistribution() * this.StandartDevitation;
        }

        public double exp(double _var)
        {
            return Math.Exp(-_var * _var / 2);
        }

        public double SimpsonRule(double lowerLim, double upperLim, int n)
        {
            double h = (upperLim - lowerLim) / (2 * n);
            double S = 0;
            double x = upperLim;
            double u = x + h;
            double w = x + 2 * h;
            do
            {
                double t = exp(x) + 4 * exp(u) + exp(w);
                S += t;
                x = w;
                u = u + 2 * h;
                w = w + 2 * h;
            }
            while (w < upperLim);
            return S * h / 3;
        }

        //Розрахунок імовірності
        public double Probability()
        {
            double func = (this.ConfidentialUpperLimit() - this.AverageTime() / this.StandartDevitation);
            return Math.Round(SimpsonRule(0, func, 20) / Math.Sqrt(2 * Math.PI),3);
        }

        public double Probability(double TimeChain)
        {
            double dev = this.StandartDevitation;
            double func = (TimeChain - this.AverageTime() / this.StandartDevitation);
            return Math.Round(SimpsonRule(0, func, 10) / Math.Sqrt(2 * Math.PI),3);
        }

        //Розрахунок імовірності, коли час задано інтервалом
        public double ProbabilityInterSpace(double upLim, double lowLim)
        {
            double func1 = (upLim - AverageTime() / this.StandartDevitation);
            double func2 = (lowLim - AverageTime() / this.StandartDevitation);
            double f1 = Math.Round(SimpsonRule(0, func1, 10) / Math.Sqrt(2 * Math.PI),2);
            double f2=Math.Round(SimpsonRule(0, func2, 10) / Math.Sqrt(2 * Math.PI),2);
            return f1-f2;
        }           
    }
}
